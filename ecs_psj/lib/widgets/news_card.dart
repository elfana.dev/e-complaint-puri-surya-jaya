import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsCard extends StatelessWidget {
  final String imageUrl;
  final String newsTitle;
  final String newsDesc;
  final String articleUrl;

  NewsCard({this.newsDesc, this.newsTitle, this.imageUrl, this.articleUrl});

  @override
  Widget build(BuildContext context) {
    launchUrl(String url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        //   throw (url);
        print('error');
      }
    }

    return InkWell(
      onTap: () {
        launchUrl(articleUrl);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Container(
          height: 200.0,
          width: 350.0,
          decoration: BoxDecoration(
            color: Colors.grey[200],
          ),
          child: Wrap(
            children: [
              Column(
                children: <Widget>[
                  Stack(
                    children: [
                      Image.asset(
                        imageUrl,
                        width: 350.0,
                        height: 102.0,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Text(
                    newsTitle,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(height: 11),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Text(
                      newsDesc,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
