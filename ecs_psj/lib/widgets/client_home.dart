import 'package:ecs_psj/pages/client_pages/client_explore.dart';
import 'package:ecs_psj/pages/client_pages/client_dashboard.dart';
import 'package:ecs_psj/pages/client_pages/client_profile.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flutter/material.dart';

class ClientHomePage extends StatefulWidget {
  ClientHomePage({Key key}) : super(key: key);

  @override
  _ClientHomePageState createState() => _ClientHomePageState();
}

class _ClientHomePageState extends State<ClientHomePage> {
  PageController _pageController = PageController();

  List<Widget> _screens = [
    ClientMainPage(),
    ClientExplorePage(),
    ClientProfilePage(),
  ];

  int _selectedIndex = 0;

  void _onPageChanged(int index) {
    setState(
      () {
        _selectedIndex = index;
      },
    );
  }

  void _onItemTapped(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        key: Key("pageView"),
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(15),
          topLeft: Radius.circular(15),
        ),
        child: BottomNavigationBar(
          key: Key("navigationBarItems"),
          unselectedItemColor: Colors.grey,
          selectedItemColor: mainColor,
          selectedFontSize: 12.0,
          backgroundColor: Colors.white,
          elevation: 10.0,
          type: BottomNavigationBarType.fixed,
          onTap: _onItemTapped,
          currentIndex: _selectedIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.dashboard,
                size: 24.0,
              ),
              label: 'Dashboard',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.explore,
                size: 24.0,
              ),
              label: 'Explore',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                size: 24.0,
              ),
              label: 'Profile',
            ),
          ],
        ),
      ),
    );
  }
}
