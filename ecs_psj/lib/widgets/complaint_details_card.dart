import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/widgets/loading.dart';
import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/services/database.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ComplaintDetailsCard extends StatefulWidget {
  final Complaints complaints;
  final DatabaseService database;

  ComplaintDetailsCard({this.complaints, this.database});
  @override
  _ComplaintDetailsCardState createState() => _ComplaintDetailsCardState();
}

final firestoreInstance = FirebaseFirestore.instance;

class _ComplaintDetailsCardState extends State<ComplaintDetailsCard> {
  bool isLoading = false;
  final _formKey = GlobalKey<FormState>();

  //NOTE: delete button
  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text('Error, Try Again!'),
        duration: Duration(seconds: 2),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //NOTE: delete button
    Widget _deleteButton(String id) {
      return isLoading
          ? Loading()
          : InkWell(
              onTap: () async {
                // print('deleted' + id);
                if (_formKey.currentState.validate()) {
                  setState(() => isLoading = true);
                  Future.delayed(Duration(seconds: 2)); // Duration to wait
                  dynamic result = DatabaseService().deleteComplaint(id);
                  Navigator.pop(context);
                  if (result == null) {
                    setState(() {
                      isLoading = false;
                      _showToast(context);
                    });
                  }
                  //  _showToast(context);
                }
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(25),
                  ),
                  border: Border.all(
                    width: 2,
                    color: Colors.grey[200],
                    style: BorderStyle.solid,
                  ),
                ),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 14),
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(24.0),
                  ),
                  child: Text(
                    'Delete Complaint',
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            );
    }

    // print('complaint' + complaints.id);
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_sharp,
            color: Colors.white,
          ),
        ),
        title: Text(
          "Complaint Details",
          style: GoogleFonts.poppins(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
        backgroundColor: secondaryColor,
      ),
      body: Form(
        key: _formKey,
        child: Container(
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      /* user's details */
                      Container(
                        padding: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              '${widget.complaints.user.fullName}', // name of complaint resident
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              '${widget.complaints.user.email}', // user resident's email
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              '${widget.complaints.user.phoneNo}', // user resident phone number
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Container(
                          padding: EdgeInsets.all(25.0),
                          width: screenWidth * 0.90,
                          height: 340.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.grey[200],
                              width: 2.0,
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Complaint details",
                                style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xFF172E7C),
                                ),
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Status",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 55),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    '${widget.complaints.status}',
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint DocID",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    '${widget.complaints.id}',
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint Type",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    '${widget.complaints.complaintType}',
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Sub-type",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    '${widget.complaints.complaintSubType}',
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Taman Location",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    '${widget.complaints.complaintTaman}',
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint Location",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Flexible(
                                    child: RichText(
                                      overflow: TextOverflow.ellipsis,
                                      strutStyle: StrutStyle(fontSize: 12.0),
                                      text: TextSpan(
                                          style: GoogleFonts.poppins(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black,
                                          ),
                                          text:
                                              "${widget.complaints.complaintLocation}"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint Date",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 35),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    '${widget.complaints.complaintDate}', // Date of complaint submission
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Remarks",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Flexible(
                                    child: RichText(
                                      overflow: TextOverflow.ellipsis,
                                      strutStyle: StrutStyle(fontSize: 12.0),
                                      text: TextSpan(
                                          style: GoogleFonts.poppins(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black,
                                          ),
                                          text:
                                              "${widget.complaints.complaintRemarks}"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      Divider(
                        color: Color(0xFFCCCCCC),
                        height: 10,
                        thickness: 15,
                      ),
                      Container(
                        padding: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Image of Complaint",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 20),
                            Container(
                              height: 250.0,
                              width: 250.0,
                              child: Image.file(
                                File('${widget.complaints.complaintImageUrl}'),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      Divider(
                        color: Color(0xFFCCCCCC),
                        height: 10,
                        thickness: 15,
                      ),
                      Container(
                        padding: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Feedback",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 15),
                            Container(
                              padding: EdgeInsets.all(25.0),
                              width: screenWidth * 0.90,
                              height: 180.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                                border: Border.all(
                                  color: Colors.grey[200],
                                  width: 2.0,
                                ),
                              ),
                              child: Column(
                                //  crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "- Thank you for submitting! -",
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(height: 15),
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        '${widget.complaints.feedback}',
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      /*  Text(":"),
                                      SizedBox(width: 10),
                                      Text(
                                        "Mr Fajar Nugraha",
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        "Phone No",
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                      Text(":"),
                                      SizedBox(width: 10),
                                      Text(
                                        "0879-8765-9900",
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                      ),*/
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 110),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Divider(
                    color: Color(0xFFCCCCCC),
                    height: 10,
                    thickness: 15,
                  ),
                  _deleteButton(widget.complaints.id),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget formTitle(String title) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: Text(
      title,
      style: GoogleFonts.poppins(
        fontSize: 20,
        fontWeight: FontWeight.w600,
        color: Color(0xFF172E7C),
      ),
    ),
  );
}
