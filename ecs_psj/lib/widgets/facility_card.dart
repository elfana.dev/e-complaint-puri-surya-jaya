import 'package:flutter/material.dart';

class FacilityCard extends StatelessWidget {
  final String imageUrl;
  final String facilityTitle;
  final String facilityDesc;

  FacilityCard({this.facilityDesc, this.facilityTitle, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
        height: 250.0,
        width: 300.0,
        decoration: BoxDecoration(
          color: Colors.grey[200],
        ),
        child: Wrap(
          children: [
            Column(
              children: <Widget>[
                Stack(
                  children: [
                    Image.asset(
                      imageUrl,
                      width: 300.0,
                      height: 102.0,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
                SizedBox(height: 11),
                Text(
                  facilityTitle,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16,
                  ),
                ),
                SizedBox(height: 11),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    facilityDesc,
                    style: TextStyle(
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
