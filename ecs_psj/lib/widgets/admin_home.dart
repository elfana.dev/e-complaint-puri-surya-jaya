import 'package:ecs_psj/pages/cs_admin_pages/admin_main_page.dart';
import 'package:ecs_psj/pages/cs_admin_pages/admin_profile.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flutter/material.dart';

class AdminHomePage extends StatefulWidget {
  AdminHomePage({Key key}) : super(key: key);

  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  PageController _pageController = PageController();

  List<Widget> _screens = [
    MainPageAdmin(),
    AdminProfile(),
  ];

  int _selectedIndex = 0;

  void _onPageChanged(int index) {
    setState(
      () {
        _selectedIndex = index;
      },
    );
  }

  void _onItemTapped(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        key: Key("pageView"),
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(15),
          topLeft: Radius.circular(15),
        ),
        child: BottomNavigationBar(
          key: Key("navigationBarItems"),
          unselectedItemColor: Colors.grey,
          selectedItemColor: mainColor,
          selectedFontSize: 12.0,
          backgroundColor: Colors.white,
          elevation: 10.0,
          type: BottomNavigationBarType.fixed,
          onTap: _onItemTapped,
          currentIndex: _selectedIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.dashboard,
                size: 24.0,
              ),
              label: 'Dashboard',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                size: 24.0,
              ),
              label: 'Profile',
            ),
          ],
        ),
      ),
    );
  }
}
