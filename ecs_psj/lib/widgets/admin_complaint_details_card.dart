import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/services/database.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:ecs_psj/pages/cs_admin_pages/admin_email_feedback.dart';
import 'package:ecs_psj/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:async';

class AdminComplaintDetailsCard extends StatefulWidget {
  final Complaints complaints;

  AdminComplaintDetailsCard({this.complaints});

  @override
  _AdminComplaintDetailsCardState createState() =>
      _AdminComplaintDetailsCardState();
}

class _AdminComplaintDetailsCardState extends State<AdminComplaintDetailsCard> {
  final firestoreInstance = FirebaseFirestore.instance;
  final CollectionReference complaintCollection =
      FirebaseFirestore.instance.collection('complaints');
  final _formKey = GlobalKey<FormState>();
  final _bodyKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<FormState>();

  TextEditingController _feedbackController = TextEditingController();
  List<String> attachments = [];
  bool isHTML = false;

  final List<String> cStatus = ['In Progress', 'Completed', 'Pending'];
  bool isLoading = false;
  String _currentStatus;
  String _currentFeedback;
  String txt = '';

  //NOTE: delete button
  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text('Error, Try Again!'),
        duration: Duration(seconds: 2),
      ),
    );
  }

  //NOTE: delete button
  Widget _deleteButton(String id) {
    return isLoading
        ? Loading()
        : InkWell(
            onTap: () async {
              // print('deleted' + id);
              if (_bodyKey.currentState.validate()) {
                setState(() => isLoading = true);
                Future.delayed(Duration(seconds: 2)); // Duration to wait
                dynamic result = DatabaseService().deleteComplaint(id);
                Navigator.pop(context);
                if (result == null) {
                  setState(() {
                    isLoading = false;
                    _showToast(context);
                  });
                }
                //  _showToast(context);
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 14),
              margin: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(24.0),
              ),
              child: Text(
                'Delete Complaint',
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
          );
  }

  //NOTE: Function to update complaint status
  updateComplaintStatus(String id, String status) async {
    await complaintCollection.doc(id).update({
      'status': status,
    });
    print('status updated' + id);
  }

  //NOTE: Function to update complaint feedback
  updateComplaintFeedback(String id, String feedback) async {
    await complaintCollection.doc(id).update({
      'feedback': _feedbackController.text,
    });
    print('status updated' + id);
  }

  @override
  Widget build(BuildContext context) {
    //NOTE: ShowDialog for Edit Feedback
    void _showEditFeedback() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(height: 15),
                    Text(
                      'Update Complaint Feedback',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: TextFormField(
                        controller: _feedbackController,
                        maxLines: 5,
                        decoration: InputDecoration(
                          hintText: 'Please type new feedback',
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(color: Colors.black),
                          ),
                        ),
                        validator: (input) => input.trim().isEmpty
                            ? 'Please enter new feedback'
                            : null,
                        onSaved: (input) => _currentFeedback = input,
                        initialValue: _currentFeedback,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    RaisedButton(
                      color: secondaryColor,
                      child: Text(
                        'Update',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          updateComplaintFeedback(
                            '${widget.complaints.id}',
                            _currentFeedback,
                          );
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
            );
          });
    }

    //NOTE: ShowDialog for Edit Status
    void _showEditStatus() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(height: 15),
                    Text(
                      'Update Complaint Status',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 70.0,
                      width: 300.0,
                      child: DropdownButtonFormField(
                        onChanged: (val) {
                          setState(() {
                            _currentStatus = val;
                          });
                        },
                        items: cStatus.map((status) {
                          return DropdownMenuItem(
                            value: status,
                            child: Text('$status'),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          hintText: "Update Status",
                          hintStyle: TextStyle(
                            fontSize: 14,
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                        ),
                        validator: (input) => _currentStatus == null
                            ? 'Please select the status'
                            : null,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    RaisedButton(
                      color: secondaryColor,
                      child: Text(
                        'Update',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () async {
                        //   updateComplaintStatus(
                        //    '${widget.complaints.id}',
                        //   _currentStatus,);
                        if (_formKey.currentState.validate()) {
                          //  setState(() => isLoading = true);
                          updateComplaintStatus(
                            '${widget.complaints.id}',
                            _currentStatus,
                          );
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
            );
          });
    }

    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_sharp,
            color: Colors.white,
          ),
        ),
        title: Text(
          "Complaint Details",
          style: GoogleFonts.poppins(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
        backgroundColor: mainColor,
      ),
      body: Form(
        key: _bodyKey,
        child: Container(
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      /* user's details */
                      Container(
                        padding: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${widget.complaints.user.fullName}", // name of complaint resident
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              '${widget.complaints.user.email}', // user resident's email
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              "${widget.complaints.user.phoneNo}", // user resident phone number
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Container(
                          padding: EdgeInsets.all(25.0),
                          width: screenWidth * 0.90,
                          height: 340.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.grey[200],
                              width: 2.0,
                            ),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Complaint details",
                                style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xFF172E7C),
                                ),
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Status",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 55),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.status}",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint DocID",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.id}",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint Type",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.complaintType}",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Sub-type",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.complaintSubType}",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Taman Location",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.complaintTaman}",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint Location",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.complaintLocation}",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Complaint Date",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 35),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Text(
                                    "${widget.complaints.complaintDate}", // Date of complaint submission
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    "Remarks",
                                    style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(":"),
                                  SizedBox(width: 10),
                                  Flexible(
                                    child: RichText(
                                      overflow: TextOverflow.ellipsis,
                                      strutStyle: StrutStyle(fontSize: 12.0),
                                      text: TextSpan(
                                          style: GoogleFonts.poppins(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black,
                                          ),
                                          text:
                                              "${widget.complaints.complaintRemarks}"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Center(
                        child: Container(
                          width: 150.0,
                          height: 40.0,
                          child: FlatButton(
                            splashColor: Colors.red,
                            color: secondaryColor,
                            textColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                color: Colors.blue,
                                width: 1,
                                style: BorderStyle.solid,
                              ),
                              borderRadius: BorderRadius.circular(24.0),
                            ),
                            child: Text(
                              'Edit Status',
                            ),
                            onPressed: () => _showEditStatus(),
                            //   DatabaseService().updateComplaintStatus(
                            //      widget.complaints.id, 'In Progress');
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      Divider(
                        color: Color(0xFFCCCCCC),
                        height: 10,
                        thickness: 15,
                      ),
                      Container(
                        padding: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Image of Complaint",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 20),
                            Container(
                              height: 250.0,
                              width: 250.0,
                              child: Image.file(
                                File('${widget.complaints.complaintImageUrl}'),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      Divider(
                        color: Color(0xFFCCCCCC),
                        height: 10,
                        thickness: 15,
                      ),
                      Container(
                        padding: EdgeInsets.all(30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Feedback",
                              style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(height: 15),
                            Container(
                              padding: EdgeInsets.all(25.0),
                              width: screenWidth * 0.90,
                              height: 180.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                                border: Border.all(
                                  color: Colors.grey[200],
                                  width: 2.0,
                                ),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Center(
                                    child: Text(
                                      "- Thank you for submitting! -",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 15),
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        '${widget.complaints.feedback}',
                                        style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                      ),
                                      SizedBox(width: 8),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 20),
                            Center(
                              child: Container(
                                width: 150.0,
                                height: 40.0,
                                child: FlatButton(
                                  splashColor: Colors.red,
                                  color: secondaryColor,
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                      color: Colors.blue,
                                      width: 1,
                                      style: BorderStyle.solid,
                                    ),
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  child: Text(
                                    'Edit Feedback',
                                  ),
                                  onPressed: () => _showEditFeedback(),
                                  //  setState(() {
                                  //    txt = 'Edit Feedback tapped';
                                  //  });
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30),
                      Divider(
                        color: Color(0xFFCCCCCC),
                        height: 10,
                        thickness: 15,
                      ),
                      SizedBox(height: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AdminEmailFeedback(
                                  complaints: widget.complaints),
                            ),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 14),
                          margin: EdgeInsets.symmetric(
                              horizontal: 25, vertical: 15),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: secondaryColor,
                            borderRadius: BorderRadius.circular(24.0),
                          ),
                          child: Text(
                            'Send Email to Resident',
                            style: GoogleFonts.poppins(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      _deleteButton(widget.complaints.id),
                      SizedBox(
                        height: 40.0,
                      ),
                      Divider(
                        color: Color(0xFFCCCCCC),
                        height: 10,
                        thickness: 15,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget formTitle(String title) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: Text(
      title,
      style: GoogleFonts.poppins(
        fontSize: 20,
        fontWeight: FontWeight.w600,
        color: Color(0xFF172E7C),
      ),
    ),
  );
}
