import 'dart:async';
import 'package:ecs_psj/routes/onboarding_screen.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashStartState();
}

class SplashStartState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return initScreen(context);
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    var duration = Duration(seconds: 3);
    return new Timer(duration, route);
  }

  route() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OnboardingScreen(),
      ),
    );
  }

  initScreen(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 10),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(top: 60.0),
                child: Image.asset(
                  'assets/images/psj-ecs-logo.png',
                  width: 200.0,
                ),
              ),
              SizedBox(height: 15),
              Text(
                "eCS",
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 15),
              Text(
                "Puri Surya Jaya",
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(height: 15),
              Text(
                "Your 24/7 Complaint System",
                style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w300,
                  color: Colors.grey,
                ),
              ),
              SizedBox(height: 15),
              CircularProgressIndicator(
                backgroundColor: secondaryColor,
                strokeWidth: 3,
              ),
              SizedBox(height: 70),
            ],
          ),
        ),
      ),
    );
  }
}
