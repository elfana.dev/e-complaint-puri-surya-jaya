import 'package:ecs_psj/model/staff.dart';
import 'package:ecs_psj/model/user_model.dart';
import 'package:ecs_psj/routes/splash_page.dart';
import 'package:ecs_psj/services/admin_auth.dart';
import 'package:ecs_psj/services/auth_services.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(EcsPsj());
}

class EcsPsj extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<ClientModel>.value(
          value: AuthServices().user,
        ),
        StreamProvider<StaffModel>.value(
          value: AdminAuthServices().user,
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SplashPage(),
        theme: ThemeData(
          textTheme: GoogleFonts.poppinsTextTheme(),
        ),
      ),
    );
  }
}
