import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/staff.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AdminAuthServices {
  static FirebaseAuth _auth = FirebaseAuth.instance;

  Future<StaffModel> _userFromFirebaseUser(User user) async {
    if (user == null) return null;
    final ref =
        FirebaseFirestore.instance.collection('staff').doc('${user.uid}');
    final doc = await ref.get();
    return user != null
        ? StaffModel(
            uid: user.uid,
            fullName: doc.data()['fullName'],
            email: user.email,
            staffRole: doc.data()['staffRole'],
            staffPhone: doc.data()['staffPhone'],
          )
        : null;
  }


  // auth change user stream
  Stream<StaffModel> get user {
    return _auth
        .authStateChanges()
        //.map((FirebaseUser user) => _userFromFirebaseUser(user));
        .asyncMap(_userFromFirebaseUser);
  }

  // GET UID
  Future<String> getCurrentUID() async {
    return (_auth.currentUser).uid;
  }

  // GET CURRENT USER
  Future getCurrentUser() async {
    return _auth.currentUser;
  }

  Future signInAnonymously() async {
    try {
      UserCredential result = await _auth.signInAnonymously();
      User firebaseUser = result.user;
      return firebaseUser;
    } catch (e) {
      print(e.toString());

      return null;
    }
  }

  //NOTE: Firebase User SignUp
  Future signUp(String email, String password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
        email: email.trim(),
        password: password,
      ); // NOTE: added trim(), because error shows "email badly formatted"
      /*
      UserModel _user = UserModel(
        uid: result.user.uid,
        email: _authResult.user.email,
        fullName: fullName,
        phoneNo: phoneNo,
        address: address,
      );*/

      User user = result.user;
      return user;
    } catch (e) {
      print(e.toString());

      return null;
    }
  }

  //Firebase User SignIn
  Future signIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email.trim(), password: password);

      User user = result.user;
      return user;
    } catch (e) {
      print(e.toString);

      return null;
    }
  }

  //NOTE: Firebase User Sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error.toString());
      return null;
    }
  }
}
