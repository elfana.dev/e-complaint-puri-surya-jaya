import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/model/user_model.dart';

class OurDatabase {
  final String uid;

  OurDatabase({this.uid});

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  Future<String> createUser(ClientModel user) async {
    String retVal = "error";

    try {
      await _firestore.collection("users").doc(user.uid).set(user.toJson());
      /*
      await _firestore.collection("users").doc(user.uid).set({
        'fullName': user.fullName,
        'phoneNo': user.phoneNo,
        'address': user.address,
        'email': user.email,
      });*/
      retVal = "success";
    } catch (e) {
      print(e);
    }
    return retVal;
  }

  Future<ClientModel> getUser(String uid) async {
    ClientModel retVal;

    try {
      DocumentSnapshot _docSnapshot =
          await _firestore.collection("users").doc(uid).get();
      retVal = ClientModel.fromDocumentSnapshot(doc: _docSnapshot);
    } catch (e) {
      print(e);
    }

    return retVal;
  }

  Future updateUserInfo(
      String fullName, String phoneNo, String email, String address) async {
    ClientModel user;
    return await userCollection.doc(user.uid).set({
      'fullName': fullName,
      'email': email,
      'phoneNo': phoneNo,
      'address': address,
    });
  }

  Stream<QuerySnapshot> get users {
    return userCollection.snapshots();
  }

  Future<ClientModel> getUserInfo(String uid) async {
    ClientModel user = ClientModel();
    try {
      DocumentSnapshot _docSnapshot =
          await _firestore.collection("users").doc(uid).get();
      user.uid = uid;
      user.phoneNo = _docSnapshot.data()["phoneNo"];
      user.address = _docSnapshot.data()["address"];
      user.fullName = _docSnapshot.data()["fullName"];
      user.email = _docSnapshot.data()["email"];
    } catch (e) {
      print(e);
    }
    return user;
  }
}

// Complaint Database

class DatabaseService {
  final String uid;

  DatabaseService({this.uid});

  // collection reference
  final CollectionReference complaintCollection =
      FirebaseFirestore.instance.collection('complaints');

  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  //NOTE: Function to update resident/client profile details
  updateClientProfile(
      String id, String fullName, String phoneNo, String address) async {
    await userCollection.doc(id).update({
      'fullName': fullName,
      'phoneNo': phoneNo,
      'address': address,
    });
  }

  //NOTE: Function to delete complaint
  deleteComplaint(String id) async {
    await complaintCollection.doc(id).delete();
    print('complaint deleted' + id);
  }

  //NOTE: Function to update complaint status
  updateComplaintStatus(String id, String status) async {
    await complaintCollection.doc(id).update({
      'status': status,
    });
    print('status updated' + id);
  }

  //NOTE: Function to update complaint feedback
  updateComplaintFeedback(String id, String feedback) async {
    await complaintCollection.doc(id).update({
      'feedback': feedback,
    });
    print('feedback updated' + id);
  }

  //NOTE: complaint list from snapshot
  List<Complaints> _complaintListFromSnapshot(QuerySnapshot snapshot) {
    print(snapshot.docs.length);
    return snapshot.docs.map((doc) {
      print('print complaint docid ' + doc.id);
      return Complaints(
        id: doc.id,
        //  userEmail: doc.data()['user'] ?? '',
        //  userName: doc.data()['userName'] ?? '',
        //  userPhoneNo: doc.data()['userPhoneNo'] ?? '',
        status: doc.data()['status'] ?? 'Submitted',
        feedback: doc.data()['feedback'] ?? 'Please wait',
        complaintType: doc.data()['complaintType'] ?? '',
        complaintSubType: doc.data()['complaintSubType'] ?? '',
        complaintLocation: doc.data()['complaintLocation'] ?? '',
        complaintTaman: doc.data()['tamanLoc'] ?? '',
        complaintDate: doc.data()['date'] ?? '',
        complaintRemarks: doc.data()['complaintRemarks'] ?? '',
        complaintImageUrl: doc.data()['complaintImage'] ?? 'No Image',
        user: ClientModel.fromData(doc.data()['user']),
      );
    }).toList();
  }

  // get complaint's stream based on currentUser
  Stream<List<Complaints>> get complaints {
    print('complaint - uid - $uid');
    final snapshot =
        complaintCollection.where('uid', isEqualTo: uid).snapshots();
    print(snapshot.length);
    return snapshot.map(_complaintListFromSnapshot);
  }
}
