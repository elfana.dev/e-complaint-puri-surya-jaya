import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class DataController extends GetxController {
  Future getComplaintQuery(String collection) async {
    final FirebaseFirestore dataComplaint = FirebaseFirestore.instance;
    QuerySnapshot complaintSnapshot =
        await dataComplaint.collection(collection).get();
    return complaintSnapshot.docs;
  }

  Future queryComplaint(String queryString) async {
    return FirebaseFirestore.instance
        .collection('complaints')
        .where(
          'status',
          isEqualTo: queryString,
        )
        .get();
  }
}
