import 'package:ecs_psj/model/user_model.dart';
import 'package:ecs_psj/routes/splash_page.dart';
import 'package:ecs_psj/widgets/client_home.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<ClientModel>(context);

    if (user == null) {
      return SplashPage();
    } else {
      return ClientHomePage();
    }
  }
}
