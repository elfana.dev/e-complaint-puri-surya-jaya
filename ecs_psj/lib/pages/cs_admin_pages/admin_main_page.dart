import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/staff.dart';
import 'package:ecs_psj/pages/cs_admin_pages/admin_complaint_list.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainPageAdmin extends StatefulWidget {
  @override
  _MainPageAdminState createState() => _MainPageAdminState();
}

class _MainPageAdminState extends State<MainPageAdmin>
    with TickerProviderStateMixin {
  //NOTE: DocsCount of Submitted
  void countSubmitted() async {
    QuerySnapshot _myDocSubmitted = await FirebaseFirestore.instance
        .collection('complaints')
        .where('status', isEqualTo: 'Submitted')
        .get();
    List<DocumentSnapshot> _myDocCount = _myDocSubmitted.docs;

    print(_myDocCount.length); // Count of Documents in Collection
  }

  //NOTE: DocsCount of InProgress
  void countInProgress() async {
    QuerySnapshot _myDocInProgress = await FirebaseFirestore.instance
        .collection('complaints')
        .where('status', isEqualTo: 'In Progress')
        .get();
    List<DocumentSnapshot> _myDocCount = _myDocInProgress.docs;
    print(_myDocCount.length); // Count of Documents in Collection
  }

  //NOTE: DocsCount of Completed
  void countCompleted() async {
    QuerySnapshot _myDocCompleted = await FirebaseFirestore.instance
        .collection('complaints')
        .where('status', isEqualTo: 'Completed')
        .get();
    List<DocumentSnapshot> _myDocCount = _myDocCompleted.docs;
    print(_myDocCount.length.toString()); // Count of Documents in Collection
  }

  final staffRef = FirebaseFirestore.instance.collection('staff');
  final submittedComplaint = FirebaseFirestore.instance
      .collection('complaints')
      .where('status', isEqualTo: 'Submitted')
      .get();

  final inProgressComplaint = FirebaseFirestore.instance
      .collection('complaints')
      .where('status', isEqualTo: 'In Progress')
      .get();

  final completedComplaint = FirebaseFirestore.instance
      .collection('complaints')
      .where('status', isEqualTo: 'Completed')
      .get();

  @override
  Widget build(BuildContext context) {
    final staffModel = Provider.of<StaffModel>(context);
    if (staffModel == null) {
      return Center(
        child: SizedBox(
          height: 100.0,
          width: 100.0,
          child: CircularProgressIndicator(
            backgroundColor: secondaryColor,
          ),
        ),
      );
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: mainColor,
        centerTitle: true,
        title: Text('eCS'),
      ),
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              children: <Widget>[
                Container(
                  height: 80.0,
                  alignment: Alignment.topCenter,
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${staffModel.fullName}',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.person_pin,
                            color: secondaryColor,
                            size: 20,
                          ),
                          SizedBox(width: 5),
                          Text(
                            '${staffModel.staffRole}',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: secondaryColor,
                            ),
                          ),
                          SizedBox(width: 10),
                          Icon(
                            Icons.phone_android,
                            color: secondaryColor,
                            size: 20,
                          ),
                          SizedBox(width: 5),
                          Text(
                            '${staffModel.staffPhone}',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: secondaryColor,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  alignment: Alignment.topCenter,
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  width: 350.0,
                  height: 200.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 2,
                        offset: Offset(0, 1),
                      ),
                    ],
                    image: DecorationImage(
                      image: AssetImage(
                        "assets/images/puri-surya-jaya-banner.jpg",
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: Wrap(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20.0),
                              margin: EdgeInsets.symmetric(vertical: 30.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Welcome, our staff!",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(height: 15),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                //NOTE: FETCH DATA HERE
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    children: <Widget>[
                      //NOTE: 'Submitted' Progress Bar
                      InkWell(
                        onTap: () async {
                          countSubmitted();
                        },
                        child: Container(
                          width: 100.0,
                          height: 120.0,
                          child: Card(
                            elevation: 2.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 5),
                                ImageIcon(
                                  AssetImage(
                                    "assets/icons/create-complaint-icon.png",
                                  ),
                                  size: 30.0,
                                  color: secondaryColor,
                                ),
                                SizedBox(height: 12),
                                Text(
                                  'Submitted',
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 2),
                                FutureBuilder(
                                  future: submittedComplaint,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      List myDocCount = snapshot.data.docs;
                                      var totalSubmitted =
                                          myDocCount.length.toString();
                                      return Text(
                                        totalSubmitted.toString(),
                                        style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF30ba55),
                                        ),
                                      );
                                    } else {
                                      return Center(
                                        child: CircularProgressIndicator(
                                          strokeWidth: 1.0,
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 18),
                      //NOTE: 'In Progress' Progress Bar
                      Container(
                        width: 100.0,
                        height: 120.0,
                        child: Card(
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 5),
                              ImageIcon(
                                AssetImage(
                                  "assets/icons/complaint-history-icon.png",
                                ),
                                size: 30.0,
                                color: secondaryColor,
                              ),
                              SizedBox(height: 12),
                              Text(
                                'In Progress',
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(height: 2),
                              FutureBuilder(
                                future: inProgressComplaint,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    List myDocCount = snapshot.data.docs;
                                    var totalInProgress =
                                        myDocCount.length.toString();
                                    return Text(
                                      totalInProgress.toString(),
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFFe8970c),
                                      ),
                                    );
                                  } else {
                                    return Center(
                                      child: CircularProgressIndicator(
                                        strokeWidth: 1.0,
                                      ),
                                    );
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 18),
                      //NOTE: 'Completed' Progress Bar
                      Container(
                        width: 100.0,
                        height: 120.0,
                        child: Card(
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 5),
                              ImageIcon(
                                AssetImage(
                                  "assets/icons/completed-icon.png",
                                ),
                                size: 30.0,
                                color: secondaryColor,
                              ),
                              SizedBox(height: 12),
                              Text(
                                'Completed',
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(height: 2),
                              FutureBuilder(
                                future: completedComplaint,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    List myDocCount = snapshot.data.docs;
                                    var totalCompleted =
                                        myDocCount.length.toString();
                                    return Text(
                                      totalCompleted.toString(),
                                      style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.red,
                                      ),
                                    );
                                  } else {
                                    return Center(
                                      child: CircularProgressIndicator(
                                        strokeWidth: 1.0,
                                      ),
                                    );
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ComplaintList(),
                      ),
                    );
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.0),
                    child: SizedBox(
                      width: 350.0,
                      height: 100.0,
                      child: Card(
                        elevation: 2.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            vertical: 10.0,
                            horizontal: 18.0,
                          ),
                          margin: EdgeInsets.only(top: 5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ImageIcon(
                                AssetImage(
                                  "assets/icons/complaint-status-icon.png",
                                ),
                                size: 50.0,
                                color: secondaryColor,
                              ),
                              SizedBox(width: 24),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(height: 8),
                                  Text(
                                    'Complaint List',
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(height: 2),
                                  Text(
                                    "See Complaint List",
                                    style: TextStyle(
                                      fontSize: 12.0,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                              Spacer(),
                              Icon(Icons.keyboard_arrow_right),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
