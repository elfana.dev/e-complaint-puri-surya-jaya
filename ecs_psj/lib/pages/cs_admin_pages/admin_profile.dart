import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/staff.dart';
import 'package:ecs_psj/routes/onboarding_screen.dart';
import 'package:ecs_psj/services/admin_auth.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class AdminProfile extends StatefulWidget {
  final String uid;

  AdminProfile({this.uid});
  @override
  _AdminProfileState createState() => _AdminProfileState();
}

class _AdminProfileState extends State<AdminProfile> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  final AdminAuthServices _authAdmin = AdminAuthServices();
  final firestoreInstance = FirebaseFirestore.instance;
  final CollectionReference staffCollection =
      FirebaseFirestore.instance.collection('staff');
  final _formKey = GlobalKey<FormState>();
  String _currentName, _currentPhone;

  //NOTE: Function to update user's details/information
  updateUserProfile(String id, String name, String phone) async {
    await staffCollection.doc(id).update({
      'fullName': _nameController.text,
      'staffPhone': _phoneController.text,
    });
    print('profile info updated ' + id);
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Confirmation',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text('Are you sure want to logout?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Confirm',
                style: TextStyle(
                  fontSize: 14,
                  color: secondaryColor,
                ),
              ),
              onPressed: () async {
                print('Confirmed');
                _authAdmin.signOut().then(
                      (result) => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => OnboardingScreen(),
                        ),
                      ),
                    );
              },
            ),
          ],
        );
      },
    );
  }

  User currentUser;
  void getCurrentUser() async {
    currentUser = FirebaseAuth.instance.currentUser;
  }

  launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      //   throw (url);
      print('error');
    }
  }

  @override
  Widget build(BuildContext context) {
    final staff = Provider.of<StaffModel>(context);

    //NOTE: ShowDialog for Edit Feedback
    void _showEditProfile() {
      showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 50),
                    Text(
                      'Update Your Profile Information',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextFormField(
                        controller: _nameController,
                        maxLines: 1,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.person,
                            color: secondaryColor,
                          ),
                          labelText: "Full Name",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: secondaryColor,
                            ),
                          ),
                        ),
                        validator: (input) => input.trim().isEmpty
                            ? 'Please enter new full name'
                            : null,
                        onSaved: (input) => _currentName = input,
                        initialValue: _currentName,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextFormField(
                        controller: _phoneController,
                        maxLines: 1,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.phone_android_outlined,
                            color: secondaryColor,
                          ),
                          labelText: "Phone No",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: secondaryColor,
                            ),
                          ),
                        ),
                        validator: (input) => input.trim().isEmpty
                            ? 'Please enter new phone number'
                            : null,
                        onSaved: (input) => _currentPhone = input,
                        initialValue: _currentPhone,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    RaisedButton(
                      color: mainColor,
                      child: Text(
                        'Update',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          updateUserProfile(
                            '${staff.uid}',
                            _currentName,
                            _currentPhone,
                          );
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
            );
          });
    }

    return Scaffold(
      backgroundColor: mainColor,
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 30.0),
                Text(
                  "My Account",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 40.0),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(30.0),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 30),
                      Card(
                        elevation: 4.0,
                        margin:
                            const EdgeInsets.fromLTRB(32.0, 8.0, 32.0, 16.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(
                                Icons.verified_user,
                                color: secondaryColor,
                              ),
                              title: Text(
                                "Profile Details",
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right),
                              onTap: () {
                                _showEditProfile();
                              },
                            ),
                            Container(
                              width: double.infinity,
                              height: 1.0,
                              color: Colors.grey,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.message_outlined,
                                color: secondaryColor,
                              ),
                              title: Text(
                                "Privacy Policy",
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right),
                              onTap: () {
                                launchUrl(
                                  'https://www.freeprivacypolicy.com/live/a8910676-530c-4d10-8af6-feaa99c76034',
                                );
                              },
                            ),
                            Container(
                              width: double.infinity,
                              height: 1.0,
                              color: Colors.grey,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.logout,
                                color: secondaryColor,
                              ),
                              title: Text(
                                "Sign Out",
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: Icon(
                                Icons.keyboard_arrow_right,
                              ),
                              onTap: () async {
                                _showMyDialog();
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 40.0),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
