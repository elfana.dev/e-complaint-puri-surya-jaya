import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/pages/cs_admin_pages/admin_list_complaint.dart';
import 'package:ecs_psj/pages/cs_admin_pages/admin_search_page.dart';
import 'package:ecs_psj/services/database.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ComplaintList extends StatefulWidget {
  @override
  _ComplaintListState createState() => _ComplaintListState();
}

class _ComplaintListState extends State<ComplaintList> {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Complaints>>.value(
      value: DatabaseService().complaints,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: true,
          backgroundColor: mainColor,
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SearchComplaint(),
                    ),
                  );
                },
                child: Icon(
                  Icons.search_outlined,
                  size: 25.0,
                ),
              ),
            ),
          ],
          title: Text(
            'Complaint list',
            style: TextStyle(fontSize: 16),
          ),
        ),
        backgroundColor: Colors.white,
        body: AdminListComplaint(),
      ),
    );
  }
}
