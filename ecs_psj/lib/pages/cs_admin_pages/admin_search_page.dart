import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/model/user_model.dart';
import 'package:ecs_psj/services/data_controller.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:ecs_psj/widgets/admin_complaint_details_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchComplaint extends StatefulWidget {
  final Complaints complaint;
  final ClientModel user;

  SearchComplaint({this.complaint, this.user});

  @override
  _SearchComplaintState createState() => _SearchComplaintState();
}

class _SearchComplaintState extends State<SearchComplaint> {
  TextEditingController searchController = TextEditingController();
  QuerySnapshot snapshotData;
  String searchString;
  bool isExecuted = false;
  @override
  Widget build(BuildContext context) {
    Widget search() {
      return ListView.builder(
          itemCount: snapshotData.docs.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Get.to(
                  AdminComplaintDetailsCard(),
                  transition: Transition.downToUp,
                  arguments: snapshotData.docs[index],
                );
              },
              child: Container(
                child: Column(
                  children: [
                    Container(
                      width: 400.0,
                      height: 180.0,
                      padding: EdgeInsets.all(10.0),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset: Offset(0, 1),
                            ),
                          ],
                        ),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 20.0),
                                    child: ImageIcon(
                                      AssetImage(
                                        'assets/icons/technical-icon.png',
                                      ),
                                      color: secondaryColor,
                                      size: 20,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Container(
                                    padding: EdgeInsets.only(top: 20.0),
                                    child: Text(
                                      snapshotData.docs[index].data()[
                                          'complaintType'], // Complaint Type
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF999999),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Container(
                                padding: EdgeInsets.only(top: 8.0),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: Icon(
                                        Icons.location_pin,
                                        color: secondaryColor,
                                        size: 20,
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      snapshotData.docs[index].data()[
                                          'tamanLoc'], // Complaint Location
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Spacer(),
                                    Container(
                                      width: 75.0,
                                      height: 25.0,
                                      padding: EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                        color: (snapshotData.docs[index]
                                                    .data()['status'] ==
                                                'Completed')
                                            ? closedComplaintColor
                                            : (snapshotData.docs[index]
                                                        .data()['status'] ==
                                                    'In Progress')
                                                ? Color(0xFFe8970c)
                                                : Color(0xFF30ba55),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(5.0),
                                        ),
                                      ),
                                      child: Text(
                                        snapshotData.docs[index].data()[
                                            'status'], // Complaint Status
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Icon(
                                      Icons.calendar_today,
                                      color: secondaryColor,
                                      size: 20,
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Container(
                                    padding: EdgeInsets.only(top: 5.0),
                                    child: Text(
                                      snapshotData.docs[index]
                                          .data()['date'], // Complaint Date
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  InkWell(
                                    onTap: () {},
                                    child: Container(
                                      padding: EdgeInsets.only(right: 15.0),
                                      child: Text(
                                        "Details", // Will navigate to complaint detail's page
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black,
                                          decoration: TextDecoration.underline,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Container(
                                child: Text(
                                  'Posted by ' +
                                      snapshotData.docs[index].data()['user']
                                          ['fullName'], // Complaint Type
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFF999999),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: secondaryColor,
        child: Icon(
          Icons.clear,
          color: Colors.white,
        ),
        onPressed: () {
          setState(() {
            isExecuted = false;
          });
        },
      ),
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: mainColor,
        automaticallyImplyLeading: true,
        actions: [
          GetBuilder<DataController>(
            init: DataController(),
            builder: (val) {
              return GestureDetector(
                onTap: () {
                  val.queryComplaint(searchController.text).then((value) {
                    snapshotData = value;
                    setState(() {
                      isExecuted = true;
                    });
                  });
                },
                child: Icon(
                  Icons.search_outlined,
                  size: 25.0,
                ),
              );
            },
          ),
        ],
        title: TextField(
          controller: searchController,
          style: TextStyle(
            color: Colors.white,
          ),
          decoration: InputDecoration.collapsed(
            hintText: 'Search Complaint',
            hintStyle: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
      body: isExecuted
          ? search()
          : Center(
              child: Container(
              child: Text(
                'Start searching',
              ),
            )),
    );
  }
}
