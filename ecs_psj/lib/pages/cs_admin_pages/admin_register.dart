import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/pages/cs_admin_pages/admin_login.dart';
import 'package:ecs_psj/services/admin_auth.dart';
import 'package:flutter/material.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:google_fonts/google_fonts.dart';

class AdminRegisterPage extends StatefulWidget {
  AdminRegisterPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AdminRegisterPageState createState() => _AdminRegisterPageState();
}

final List<String> _staffRole = [
  'Admin',
  'Customer Service',
  'Workman',
];

String _role;
final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
final AdminAuthServices _auth = AdminAuthServices();

class _AdminRegisterPageState extends State<AdminRegisterPage> {
  TextEditingController _fullNameController = TextEditingController(text: "");
  TextEditingController _adminEmailController = TextEditingController(text: "");
  TextEditingController _adminPassController = TextEditingController(text: "");
  TextEditingController _staffPhoneController = TextEditingController(text: "");
  TextEditingController _confirmPasswordController =
      TextEditingController(text: "");

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  Widget _userPasswordField({bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Password',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: _confirmPasswordController,
            obscureText: isPassword,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          ),
          Text(
            'Confirm Password',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: _adminPassController,
            obscureText: isPassword,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _userEmailField() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Email',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: _adminEmailController,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _nameField() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Full Name',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: _fullNameController,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _submitButton() {
    return InkWell(
      onTap: () async {
        if (_registerFormKey.currentState.validate()) {
          if (_adminPassController.text == _confirmPasswordController.text) {
            _auth
                .signUp(_adminEmailController.text, _adminPassController.text)
                .then((currentUser) => FirebaseFirestore.instance
                    .collection("staff")
                    .doc(currentUser.uid)
                    .set({
                      "uid": currentUser.uid,
                      "fullName": _fullNameController.text,
                      "staffPhone": _staffPhoneController.text,
                      "email": _adminEmailController.text,
                      'staffRole': _role,
                    })
                    .then(
                      (result) => {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginAdmin(),
                            ),
                            (_) => false),
                        _fullNameController.clear(),
                        _staffPhoneController.clear(),
                        _adminEmailController.clear(),
                        _adminPassController.clear(),
                        _confirmPasswordController.clear(),
                      },
                    )
                    .catchError((err) => print(err)))
                .catchError((err) => print(err));
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Error"),
                    content: Text("The passwords do not match"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("Close"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  );
                });
          }
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey.shade200,
              offset: Offset(2, 4),
              blurRadius: 5,
              spreadRadius: 2,
            )
          ],
          color: mainColor,
        ),
        child: Text(
          'Create Account',
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _title() {
    return Text(
      'Register Your Account',
      style: GoogleFonts.poppins(
        fontSize: 16,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget _staffPhoneNoField() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Phone No',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            keyboardType: TextInputType.phone,
            controller: _staffPhoneController,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          )
        ],
      ),
    );
  }

  Widget _userCredentialsWidget() {
    return Form(
      key: _registerFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _nameField(),
          _staffPhoneNoField(),
          _userEmailField(),
          _userPasswordField(isPassword: true),
          // STAFF ROLE FIELD
          SizedBox(height: 10),
          Text(
            'Staff Role',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          SizedBox(height: 10),
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(
                color: Colors.black,
              ),
            ),
            child: DropdownButtonFormField(
              onChanged: (value) {
                setState(() {
                  _role = value;
                });
              },
              items: _staffRole.map((String role) {
                return DropdownMenuItem(
                  value: role,
                  child: Text(
                    role,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                    ),
                  ),
                );
              }).toList(),
              icon: Icon(
                Icons.arrow_drop_down_outlined,
                size: 22,
              ),
              decoration: InputDecoration(
                hintText: "Select your role",
                hintStyle: TextStyle(
                  fontSize: 14,
                ),
              ),
              validator: (input) =>
                  _role == null ? 'Please select your role' : null,
            ),
          ),
        ],
      ),
    );
  }

  Widget _tncField() {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.all(15),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'I Agree to',
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              'Privacy Policy',
              style: TextStyle(
                color: secondaryColor,
                fontSize: 13,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        height: height,
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: height * .1),
                    Align(
                      alignment: Alignment.center,
                      child: _title(),
                    ),
                    SizedBox(height: 50),
                    // STAFF CREDENTIALS FIELD
                    _userCredentialsWidget(),
                    SizedBox(height: 30),
                    _submitButton(),
                    _tncField(),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 40,
              left: 0,
              child: _backButton(),
            ),
          ],
        ),
      ),
    );
  }
}
