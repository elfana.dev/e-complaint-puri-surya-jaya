import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:google_fonts/google_fonts.dart';

class AdminEmailFeedback extends StatefulWidget {
  final Complaints complaints;

  AdminEmailFeedback({this.complaints});

  @override
  _AdminEmailFeedbackState createState() => _AdminEmailFeedbackState();
}

class _AdminEmailFeedbackState extends State<AdminEmailFeedback> {
  final firestoreInstance = FirebaseFirestore.instance;
  final CollectionReference complaintCollection =
      FirebaseFirestore.instance.collection('complaints');
  final _emailFormKey = GlobalKey<FormState>();
  bool _enableBtn = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController subjectController = TextEditingController();
  TextEditingController messageController = TextEditingController();

  OutlineInputBorder border = OutlineInputBorder(
      borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none);

  void showSuccessFlushBar(BuildContext context) {
    Flushbar(
      backgroundColor: Colors.grey,
      title: 'Success',
      message: 'Email Sent successfully, Please Wait....',
      icon: Icon(
        Icons.done_outline,
        size: 28,
        color: Colors.green.shade300,
      ),
      leftBarIndicatorColor: Colors.blue.shade300,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  @override
  void dispose() {
    emailController?.dispose();
    subjectController?.dispose();
    messageController?.dispose();
    super.dispose();
  }

  @override
  initState() {
    emailController = new TextEditingController();
    subjectController = new TextEditingController();
    messageController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_sharp,
            color: Colors.white,
          ),
        ),
        title: Text(
          "Send Email Feedback",
          style: GoogleFonts.poppins(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
        backgroundColor: mainColor,
      ),
      body: Container(
        child: Form(
          key: _emailFormKey,
          onChanged: (() {
            setState(() {
              _enableBtn = _emailFormKey.currentState.validate();
            });
          }),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Feedback of Complaint : ',
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      widget.complaints.id, // user resident's email
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Email to : ',
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      widget.complaints.user.email, // user resident's email
                      style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                TextFields(
                    controller: subjectController,
                    name: "Subject",
                    validator: ((value) {
                      if (value.isEmpty) {
                        return 'Name is required';
                      }
                      return null;
                    })),
                TextFields(
                    controller: emailController,
                    name: "Email",
                    validator: ((value) {
                      if (value.isEmpty) {
                        return 'Email is required';
                      } else if (!value.contains('@')) {
                        return 'Please enter a valid email address';
                      }
                      return null;
                    })),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                  child: TextFormField(
                    keyboardType: TextInputType.multiline,
                    maxLines: 8,
                    controller: messageController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      fillColor: Colors.grey[300],
                      filled: true,
                      labelText: 'Feedback Message',
                      focusedErrorBorder: border,
                      focusedBorder: border,
                      enabledBorder: border,
                      errorBorder: border,
                    ),
                    validator: ((value) {
                      if (value.isEmpty) {
                        setState(() {
                          _enableBtn = true;
                        });
                        return 'Message is required';
                      }
                      return null;
                    }),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 50.0,
                  width: 320.0,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed))
                            return Theme.of(context)
                                .colorScheme
                                .primary
                                .withOpacity(0.5);
                          else if (states.contains(MaterialState.disabled))
                            return Colors.grey;
                          return secondaryColor; // Use the component's default.
                        },
                      ),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24.0),
                        ),
                      ),
                    ),
                    onPressed: _enableBtn
                        ? (() async {
                            final Email email = Email(
                              body: messageController.text,
                              subject: subjectController.text,
                              recipients: [emailController.text],
                              isHTML: false,
                            );

                            await FlutterEmailSender.send(email);
                            showSuccessFlushBar(context);
                          })
                        : null,
                    child: Text('Submit'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TextFields extends StatelessWidget {
  final TextEditingController controller;
  final String name;
  final String Function(String) validator;
  final int maxLines;
  final TextInputType type;

  TextFields(
      {this.controller, this.name, this.validator, this.maxLines, this.type});
  @override
  Widget build(BuildContext context) {
    OutlineInputBorder border = OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide.none);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
      child: TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        keyboardType: type,
        maxLines: maxLines,
        controller: controller,
        decoration: InputDecoration(
          border: InputBorder.none,
          fillColor: Colors.grey[300],
          filled: true,
          labelText: name,
          focusedErrorBorder: border,
          focusedBorder: border,
          enabledBorder: border,
          errorBorder: border,
        ),
        validator: validator,
      ),
    );
  }
}
