import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/widgets/admin_complaint_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AdminListComplaint extends StatefulWidget {
  @override
  _AdminListComplaintState createState() => _AdminListComplaintState();
}

class _AdminListComplaintState extends State<AdminListComplaint> {
  @override
  Widget build(BuildContext context) {
    final complaint = Provider.of<List<Complaints>>(context);

    return ListView.builder(
      itemCount: complaint?.length ?? 0,
      itemBuilder: (context, index) {
        return AdminComplaintCard(complaints: complaint[index]);
      },
    );
  }
}
