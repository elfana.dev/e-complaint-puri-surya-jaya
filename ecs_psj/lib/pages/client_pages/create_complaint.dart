import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/user_model.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:uuid/uuid.dart';

enum PhotoOptions { camera, library }

class CreateComplaint extends StatefulWidget {
  final String uid;

  CreateComplaint({this.uid});

  @override
  _CreateComplaintState createState() => _CreateComplaintState();
}

class _CreateComplaintState extends State<CreateComplaint> {
  File _image;

  //NOTE: Reference to complaint's collection
  final CollectionReference complaints =
      FirebaseFirestore.instance.collection('complaints');
  FirebaseFirestore complaintFirestore = FirebaseFirestore.instance;

  TextEditingController _locationDetailsController = TextEditingController();
  TextEditingController _complaintRemarksController = TextEditingController();
  TextEditingController _dateController = TextEditingController();

  bool isLoading = false;
  // UploadComplaintPicture _upload;
  final _formkey = GlobalKey<FormState>();

  DateTime _date = DateTime.now();
  final DateFormat _dateFormatter = DateFormat('MM, dd, yyy');
  final List<String> _complaintTypes = [
    '(1E)-Electricity',
    '(2E)-PDAM/Water Supply',
    '(3W)-Trash',
    '(4O)-Others',
  ];

  final List<String> _complaintSubTypes = [
    '(1E01)-Blackout',
    '(1E02)-Gardu Listrik',
    '(1E03)-Lampu Jalan',
    '(1E04)-Lampu Taman',
    '(1E05)-Ciruit Breaker',
    '(2E01)-Air Mati',
    '(2E02)-Tandon',
    '(2E03)-Pompa Air',
    '(3W01)-Sampah belum di ambil',
    '(3W02)-Sampah berserakan',
    '(4O01)-Others',
  ];
  final List<String> _tamanList = [
    'Taman Athena',
    'Taman Boston',
    'Taman Paris A',
    'Taman Paris B',
    'Sydney Garden',
    'Taman Nagoya',
    'Taman Pasadena',
    'Main Gate',
    'Foster',
    'Others',
  ];

  String _tamans;
  String _cTypes;
  String _cSubTypes;
  String _complaintRemarks;
  String _complaintLocation;

  //NOTE: Firestore Function to create complaint
  void addComplaint(BuildContext context) async {
    var user = Provider.of<ClientModel>(context, listen: false);
    return await complaints
        .doc()
        .set({
          'uid': user.uid,
          'user': {
            'uid': user.uid,
            'fullName': user.fullName,
            'email': user.email,
            'phoneNo': user.phoneNo,
          },
          'complaintLocation': _locationDetailsController.text,
          'complaintRemarks': _complaintRemarksController.text,
          'date': _dateController.text,
          'complaintType': _cTypes,
          'complaintSubType': _cSubTypes,
          'tamanLoc': _tamans,
          'complaintImage': _image.path,
          'status': 'Submitted',
          'feedback': 'Please wait...',
        })
        .then((value) => print("Complaint Added Succesfully"))
        .catchError(
          (error) => Flushbar(
            duration: Duration(seconds: 2),
            padding: EdgeInsets.all(10.0),
            margin: EdgeInsets.symmetric(vertical: 10.0),
            borderRadius: 8,
            backgroundColor: Colors.red,
            dismissDirection: FlushbarDismissDirection.HORIZONTAL,
            forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
            title: 'Error',
            message: '$error',
          ),
        );
  }

  @override
  void dispose() {
    _dateController?.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    final userModel = Provider.of<ClientModel>(context);
    if (userModel == null) {
      return Center(
        child: SizedBox(
          height: 100.0,
          width: 100.0,
          child: CircularProgressIndicator(
            backgroundColor: secondaryColor,
          ),
        ),
      );
    }

    void showSuccessFlushBar(BuildContext context) {
      Flushbar(
        title: 'Success',
        message: 'Complaint Submitted successfully, Please Wait....',
        icon: Icon(
          Icons.done_outline,
          size: 28,
          color: Colors.green.shade300,
        ),
        leftBarIndicatorColor: Colors.blue.shade300,
        duration: Duration(seconds: 3),
      )..show(context);
    }

    //NOTE: Get image from firebase storage
    Future getImage() async {
      final imagePicker = ImagePicker();
      final pickedImage =
          await imagePicker.getImage(source: ImageSource.gallery);

      setState(() {
        _image = File(pickedImage.path);
        print('Image Path $_image');
      });
    }

    //NOTE: DATE PICKER
    _handleDatePicker() async {
      final DateTime date = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2100),
      );
      if (date != null && date != _date) {
        setState(() {
          _date = date;
        });
        _dateController.text = _dateFormatter.format(date);
      }
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        backgroundColor: secondaryColor,
        title: Text(
          'Create Complaint',
          style: TextStyle(fontSize: 16),
        ),
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Container(
            child: Form(
              key: _formkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 20.0),
                            child: ImageIcon(
                              AssetImage('assets/icons/location-icon.png'),
                              color: secondaryColor,
                            ),
                          ),
                          //NOTE: LOCATION FIELD
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[],
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 30.0),
                            child: Text(
                              'Location Details',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Container(
                        height: 60.0,
                        width: 250.0,
                        margin: EdgeInsets.only(left: 70),
                        child: DropdownButtonFormField(
                          onChanged: (value) {
                            setState(() {
                              _tamans = value;
                            });
                          },
                          items: _tamanList.map((String taman) {
                            return DropdownMenuItem(
                              value: taman,
                              child: Text(
                                taman,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                ),
                              ),
                            );
                          }).toList(),
                          icon: Icon(
                            Icons.arrow_drop_down_circle,
                            color: secondaryColor,
                            size: 22,
                          ),
                          decoration: InputDecoration(
                            hintText: "Select Taman",
                            hintStyle: TextStyle(
                              fontSize: 14,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10.0),
                              ),
                            ),
                          ),
                          validator: (input) => _tamans == null
                              ? 'Please select your taman location'
                              : null,
                        ),
                      ),
                      SizedBox(height: 15),
                      //NOTE: LOCATION / ADDRESS FIELD
                      Padding(
                        padding: EdgeInsets.only(left: 30.0),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 40.0),
                          margin: EdgeInsets.symmetric(vertical: 10.0),
                          child: TextFormField(
                            controller: _locationDetailsController,
                            maxLines: 2,
                            decoration: InputDecoration(
                              hintText: "Home block number...",
                              hintStyle: TextStyle(
                                fontSize: 14,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide: BorderSide(color: Colors.black),
                              ),
                            ),
                            validator: (input) => input.trim().isEmpty
                                ? 'Please enter location details'
                                : null,
                            onSaved: (input) => _complaintLocation = input,
                            initialValue: _complaintLocation,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Divider(
                    color: Colors.grey[200],
                    height: 10,
                    thickness: 4,
                  ),
                  //NOTE: COMPLAINT TYPE
                  SizedBox(height: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: ImageIcon(
                              AssetImage('assets/icons/message-icon.png'),
                              color: secondaryColor,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 8.0),
                            child: Text(
                              'Complaint Type',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      //NOTE: COMPLAINT FIELD
                      Container(
                        height: 60.0,
                        width: 250.0,
                        margin: EdgeInsets.only(left: 65),
                        child: DropdownButtonFormField(
                          onChanged: (value) {
                            setState(() {
                              _cTypes = value;
                            });
                          },
                          items: _complaintTypes.map((String ctype) {
                            return DropdownMenuItem(
                              value: ctype,
                              child: Text(
                                ctype,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                ),
                              ),
                            );
                          }).toList(),
                          icon: Icon(
                            Icons.arrow_drop_down_circle,
                            color: secondaryColor,
                            size: 22,
                          ),
                          decoration: InputDecoration(
                            hintText: "Select Complaint Type",
                            hintStyle: TextStyle(
                              fontSize: 14,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10.0),
                              ),
                            ),
                          ),
                          validator: (input) => _cTypes == null
                              ? 'Please select your complaint type'
                              : null,
                        ),
                      ),
                      SizedBox(height: 15),
                      //NOTE: COMPLAINT SUB TYPES
                      Container(
                        height: 60.0,
                        width: 300.0,
                        margin: EdgeInsets.only(left: 65),
                        child: DropdownButtonFormField(
                          onChanged: (value) {
                            setState(() {
                              _cSubTypes = value;
                            });
                          },
                          items: _complaintSubTypes.map((String cSubtype) {
                            return DropdownMenuItem(
                              value: cSubtype,
                              child: Text(
                                cSubtype,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                ),
                              ),
                            );
                          }).toList(),
                          icon: Icon(
                            Icons.arrow_drop_down_circle,
                            color: secondaryColor,
                            size: 22,
                          ),
                          decoration: InputDecoration(
                            hintText: "Select Sub Types",
                            hintStyle: TextStyle(
                              fontSize: 14,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10.0),
                              ),
                            ),
                          ),
                          validator: (input) => _cTypes == null
                              ? 'Please select your complaint sub-type'
                              : null,
                        ),
                      ),
                      SizedBox(height: 15),
                      //NOTE: DETAIL ADDRESS FIELD
                      Container(
                        padding: EdgeInsets.only(left: 70.0),
                        child: Text(
                          'Remarks',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 25.0),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 40.0),
                          margin: EdgeInsets.symmetric(vertical: 10.0),
                          child: TextFormField(
                            maxLength: 70,
                            controller: _complaintRemarksController,
                            maxLines: 2,
                            decoration: InputDecoration(
                              hintText: "Write your remarks...",
                              hintStyle: TextStyle(
                                fontSize: 14,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide: BorderSide(color: Colors.black),
                              ),
                            ),
                            validator: (input) => input.trim().isEmpty
                                ? 'Please enter your remarks'
                                : null,
                            onSaved: (input) => _complaintRemarks = input,
                            initialValue: _complaintRemarks,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Divider(
                    color: Colors.grey[200],
                    height: 10,
                    thickness: 4,
                  ),
                  //NOTE: PICTURE DETAILS
                  SizedBox(height: 15),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        child: ImageIcon(
                          AssetImage('assets/icons/camera-icon.png'),
                          color: secondaryColor,
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Take Picture",
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.all(8.0),
                                width: 100.0,
                                height: 100.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black,
                                  ),
                                ),
                                child: (_image != null)
                                    ? Image.file(
                                        _image,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.network(
                                        "https://d1eipm3vz40hy0.cloudfront.net/images/AMER/Customer%20service%20vs.%20customer%20support-min.jpg",
                                        fit: BoxFit.fill,
                                      ),
                              ),
                              SizedBox(width: 10),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: FlatButton(
                                  color: secondaryColor,
                                  onPressed: () {
                                    getImage();
                                  },
                                  textColor: Colors.white,
                                  child: Text('Add Image'),
                                  /*  child: PopupMenuButton<PhotoOptions>(
                                    onSelected: _optionSelected,
                                    child: Text("Add Photo"),
                                    itemBuilder: (context) => [
                                      PopupMenuItem(
                                        child: Text("Take a picture"),
                                        value: PhotoOptions.camera,
                                      ),
                                      PopupMenuItem(
                                        child:
                                            Text("Select from photo library"),
                                        value: PhotoOptions.library,
                                      )
                                    ],
                                  ),*/
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Divider(
                    color: Colors.grey[200],
                    height: 10,
                    thickness: 4,
                  ),
                  //NOTE: DATE & TIME DETAILS
                  SizedBox(height: 15),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 20.0),
                        child: ImageIcon(
                          AssetImage('assets/icons/date-icon.png'),
                          color: secondaryColor,
                        ),
                      ),
                      SizedBox(height: 20, width: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: Container(
                          width: 120.0,
                          height: 50.0,
                          child: TextFormField(
                            onTap: _handleDatePicker,
                            controller: _dateController,
                            decoration: InputDecoration(
                              labelText: "Date",
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Divider(
                    color: Colors.grey[200],
                    height: 10,
                    thickness: 4,
                  ),
                  SizedBox(height: 20),
                  Center(
                    child: isLoading
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : Container(
                            width: 180.0,
                            height: 40.0,
                            child: RaisedButton(
                              color: secondaryColor,
                              padding: EdgeInsets.symmetric(horizontal: 50),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              child: Text(
                                "Submit",
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: () {
                                addComplaint(context);
                                _locationDetailsController.text = '';
                                _complaintRemarksController.text = '';
                                _dateController.text = '';
                                _cTypes = '';
                                _cSubTypes = '';
                                _tamans = '';
                                showSuccessFlushBar(context);
                              },
                            ),
                          ),
                  ),
                  SizedBox(height: 70),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class UploadComplaintPicture {
  Future<String> uploadFile(File file) async {
    String downloadURL;

    final uuid = Uuid();
    final filePath = "/images/${uuid.v4()}.jpg";
    final storage = FirebaseStorage.instance.ref(filePath);
    final uploadTask = await storage.putFile(file);

    if (uploadTask.state == TaskState.success) {
      downloadURL =
          await FirebaseStorage.instance.ref(filePath).getDownloadURL();
    }

    return downloadURL;
  }
}
