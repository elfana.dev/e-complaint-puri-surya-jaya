import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/model/user_model.dart';
import 'package:ecs_psj/pages/client_pages/list_complaint.dart';
import 'package:ecs_psj/services/database.dart';
import 'package:flutter/material.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:provider/provider.dart';

class ComplaintHistory extends StatefulWidget {
  @override
  _ComplaintHistoryState createState() => _ComplaintHistoryState();
}

class _ComplaintHistoryState extends State<ComplaintHistory> {
  @override
  Widget build(BuildContext context) {
    final userModel = Provider.of<ClientModel>(
      context,
    );
    if (userModel == null) {
      return Center(
        child: SizedBox(
          height: 100.0,
          width: 100.0,
          child: CircularProgressIndicator(
            backgroundColor: secondaryColor,
          ),
        ),
      );
    }
    //print(userModel.uid);
    return StreamProvider<List<Complaints>>.value(
      value: DatabaseService(uid: userModel.uid).complaints,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: true,
          backgroundColor: secondaryColor,
          title: Text(
            'Complaint History',
            style: TextStyle(fontSize: 16),
          ),
        ),
        backgroundColor: Colors.white,
        body: ListComplaint(),
      ),
    );
  }
}
