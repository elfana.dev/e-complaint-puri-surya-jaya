import 'package:ecs_psj/widgets/facility_card.dart';
import 'package:ecs_psj/widgets/news_card.dart';
import 'package:flutter/material.dart';

class ClientExplorePage extends StatefulWidget {
  @override
  _ClientExplorePageState createState() => _ClientExplorePageState();
}

class _ClientExplorePageState extends State<ClientExplorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: ListView(
          children: [
            SizedBox(height: 20.0),
            //NOTE: PAGE TITLE
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Text(
                    "Explore Now",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: Colors.grey,
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            SizedBox(height: 30),
            //NOTE: FACILITY FIELD
            Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(
                "Our Facility",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(height: 2),
            Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(
                "Check our facilities!",
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0xFF82868E),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(height: 16),
            Container(
              height: 200.0,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  SizedBox(width: 24),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-school.jpg',
                    facilityTitle: 'Pembangunan Jaya School',
                    facilityDesc:
                        'Pembangunan jaya 2 School, located in Taman Vancouver',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-sportclub.jpg',
                    facilityTitle: 'Sports Club',
                    facilityDesc:
                        'The Sports Club is quite complete, from gym to swimming pool',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-swimmingpool.jpg',
                    facilityTitle: 'Swimming Pool',
                    facilityDesc:
                        'This swimming pool consists of a children`s pool and an adult pool.',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-police.jpg',
                    facilityTitle: 'Police Station',
                    facilityDesc:
                        'Residential in a safe environment, there is a police station inside.',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-mosque.jpg',
                    facilityTitle: 'Shalahuddin Mosque',
                    facilityDesc:
                        'This is now a center for Islamic dakwah through education, economics and health.',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-onegate.jpg',
                    facilityTitle: 'One Gate System',
                    facilityDesc:
                        'This one-door system is applied at the entrance to the area or residential area.',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-hall.jpg',
                    facilityTitle: 'Multipurpose Hall',
                    facilityDesc:
                        'Provides parking lots, larger rooms, considered more practical, strategic.',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-supermarket.jpg',
                    facilityTitle: 'Supermarket',
                    facilityDesc:
                        'Superindo has operated as a supermarket facility owned by Puri Surya Jaya.',
                  ),
                  SizedBox(width: 15),
                  FacilityCard(
                    imageUrl: 'assets/images/facility-foster.jpg',
                    facilityTitle: 'Food Avenue',
                    facilityDesc:
                        'Serves culinary delights ranging from Asian to European cuisine.',
                  ),
                  SizedBox(width: 15),
                ],
              ),
            ),
            //NOTE: NEWS FIELD
            SizedBox(height: 25),
            Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(
                "News",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(height: 2),
            Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(
                "Check what's new!",
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0xFF82868E),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(height: 16),
            Container(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20),
                  NewsCard(
                    articleUrl: 'https://purisuryajaya.com/promo-ramadhan/',
                    imageUrl: 'assets/images/psj-ramadhan-offer.jpg',
                    newsTitle: 'RAMADHAN SPECIAL OFFER‼️',
                    newsDesc: 'Click to read more...',
                  ),
                  SizedBox(height: 20),
                  NewsCard(
                    articleUrl:
                        'https://purisuryajaya.com/alexandria-hills-pricelist/',
                    imageUrl: 'assets/images/psj-alexandria-hills-offers.jpg',
                    newsTitle: 'ALEXANDRIA HILLS, NOW UP!',
                    newsDesc: 'Click to read more...',
                  ),
                  SizedBox(height: 20),
                  NewsCard(
                    articleUrl:
                        'https://purisuryajaya.com/funfact-3-kelebihan-jaringan-listrik-bawah-tanah/',
                    imageUrl: 'assets/images/psj-news.jpg',
                    newsTitle:
                        '#FunFact 3 Kelebihan Jaringan\nListrik Bawah Tanah',
                    newsDesc: 'Click to read more...',
                  ),
                ],
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
