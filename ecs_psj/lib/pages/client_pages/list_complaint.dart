import 'package:ecs_psj/model/complaints.dart';
import 'package:ecs_psj/widgets/complaint_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListComplaint extends StatefulWidget {
  @override
  _ListComplaintState createState() => _ListComplaintState();
}

class _ListComplaintState extends State<ListComplaint> {
  @override
  Widget build(BuildContext context) {
    final complaint = Provider.of<List<Complaints>>(context);

    return ListView.builder(
      itemCount: complaint?.length ?? 0,
      itemBuilder: (context, index) {
        return ComplaintCard(complaints: complaint[index]);
      },
    );
  }
}
