import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ecs_psj/model/user_model.dart';
import 'package:ecs_psj/pages/client_pages/login_page.dart';
import 'package:ecs_psj/services/auth_services.dart';
import 'package:ecs_psj/theme/theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ClientProfilePage extends StatefulWidget {
  final String id;

  ClientProfilePage({this.id});

  @override
  _ClientProfilePageState createState() => _ClientProfilePageState();
}

class _ClientProfilePageState extends State<ClientProfilePage> {
  final AuthServices _auth = AuthServices();
  final firestoreInstance = FirebaseFirestore.instance;
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');
  final _formKey = GlobalKey<FormState>();

  TextEditingController _clientNameController = TextEditingController();
  TextEditingController _clientPhoneController = TextEditingController();
  TextEditingController _clientAddressController = TextEditingController();

  String _currentName, _currentPhone, _currentAddress;

  User currentUser;
  void getCurrentUser() async {
    currentUser = FirebaseAuth.instance.currentUser;
  }

  void showSuccessFlushBar(BuildContext context) {
    Flushbar(
      title: 'Updated!',
      message:
          'Submitted successfully, The system will update your information....',
      icon: Icon(
        Icons.done_outline,
        size: 28,
        color: Colors.green.shade300,
      ),
      leftBarIndicatorColor: Colors.blue.shade300,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  //NOTE: Function to update user's details/information
  updateUserProfile(
      String id, String name, String phone, String address) async {
    await userCollection.doc(id).update({
      'fullName': _clientNameController.text,
      'phoneNo': _clientPhoneController.text,
      'address': _clientAddressController.text,
    });
    print('profile info updated' + id);
  }

  launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      //   throw (url);
      print('error');
    }
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Confirmation',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Text('Are you sure want to logout?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Confirm',
                style: TextStyle(
                  fontSize: 14,
                  color: secondaryColor,
                ),
              ),
              onPressed: () async {
                print('Confirmed');
                _auth.signOut().then(
                      (result) => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage(),
                        ),
                      ),
                    );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<ClientModel>(context);

    //NOTE: ShowDialog for Edit Feedback
    void _showEditProfile() {
      showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.all(10.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 50),
                    Text(
                      'Update Your Profile Information',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextFormField(
                        controller: _clientNameController,
                        maxLines: 1,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.person,
                            color: secondaryColor,
                          ),
                          labelText: "Full Name",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: secondaryColor,
                            ),
                          ),
                        ),
                        validator: (input) => input.trim().isEmpty
                            ? 'Please enter new full name'
                            : null,
                        onSaved: (input) => _currentName = input,
                        initialValue: _currentName,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextFormField(
                        controller: _clientPhoneController,
                        maxLines: 1,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.phone_android_outlined,
                            color: secondaryColor,
                          ),
                          labelText: "Phone No",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: secondaryColor,
                            ),
                          ),
                        ),
                        validator: (input) => input.trim().isEmpty
                            ? 'Please enter new phone number'
                            : null,
                        onSaved: (input) => _currentPhone = input,
                        initialValue: _currentPhone,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextFormField(
                        controller: _clientAddressController,
                        maxLines: 1,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.home_rounded,
                            color: secondaryColor,
                          ),
                          labelText: "Address",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                            borderSide: BorderSide(
                              color: secondaryColor,
                            ),
                          ),
                        ),
                        validator: (input) => input.trim().isEmpty
                            ? 'Please enter new address'
                            : null,
                        onSaved: (input) => _currentAddress = input,
                        initialValue: _currentAddress,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    RaisedButton(
                      color: secondaryColor,
                      child: Text(
                        'Update',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          updateUserProfile(
                            '${user.uid}',
                            _currentName,
                            _currentPhone,
                            _currentAddress,
                          );
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
            );
          });
    }

    return Scaffold(
      backgroundColor: secondaryColor,
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 30.0),
                Text(
                  "My Account",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 40.0),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(30.0),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 30),
                      userInformation(context),
                      SizedBox(height: 30),
                      Card(
                        elevation: 4.0,
                        margin:
                            const EdgeInsets.fromLTRB(32.0, 8.0, 32.0, 16.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(
                                Icons.verified_user,
                                color: secondaryColor,
                              ),
                              title: Text(
                                "Edit Profile",
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right),
                              onTap: () {
                                _showEditProfile();
                              },
                            ),
                            Container(
                              width: double.infinity,
                              height: 1.0,
                              color: Colors.grey,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.privacy_tip,
                                color: secondaryColor,
                              ),
                              title: Text(
                                "Privacy Policies",
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right),
                              onTap: () {
                                launchUrl(
                                  'https://www.freeprivacypolicy.com/live/a8910676-530c-4d10-8af6-feaa99c76034',
                                );
                                //       Navigator.push(
                                //         context,
                                //         MaterialPageRoute(
                                //           builder: (context) => FeedbackPage(),
                                //        ),
                                //     );
                              },
                            ),
                            Container(
                              width: double.infinity,
                              height: 1.0,
                              color: Colors.grey,
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.logout,
                                color: secondaryColor,
                              ),
                              title: Text(
                                "Sign Out",
                                style: TextStyle(color: Colors.black),
                              ),
                              trailing: Icon(
                                Icons.keyboard_arrow_right,
                              ),
                              onTap: () async {
                                _showMyDialog();
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget userInformation(BuildContext context) {
  final userModel = Provider.of<ClientModel>(context);
  if (userModel == null) {
    return Center(
      child: SizedBox(
        height: 100.0,
        width: 100.0,
        child: CircularProgressIndicator(
          backgroundColor: secondaryColor,
        ),
      ),
    );
  }
  //print(' uuserInformation ' + userModel.uid);
  return Container(
    alignment: Alignment.topCenter,
    width: 380.0,
    height: 160.0,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(12.0),
      boxShadow: [
        BoxShadow(
          color: secondaryColor.withOpacity(0.4),
          spreadRadius: 2,
          blurRadius: 2,
          offset: Offset(0, 1),
        ),
      ],
    ),
    child: ListView(
      children: [
        Container(
          padding: EdgeInsets.only(top: 20.0, left: 20.0),
          child: Text(
            "Hi, ${userModel.fullName}",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 10.0, left: 20.0),
          child: Column(
            children: [
              // User's email field
              Row(
                children: [
                  Icon(
                    Icons.mail,
                    color: secondaryColor,
                    size: 20,
                  ),
                  SizedBox(width: 8),
                  Text(
                    userModel.email ?? '',
                  ),
                ],
              ),
              SizedBox(height: 10),
              // User's phone no field
              Row(
                children: [
                  Icon(
                    Icons.phone_android,
                    color: secondaryColor,
                    size: 20,
                  ),
                  SizedBox(width: 8),
                  Text(
                    '${userModel.phoneNo}',
                  ),
                ],
              ),
              SizedBox(height: 10),
              // User's address field
              Row(
                children: [
                  Icon(
                    Icons.home_filled,
                    color: secondaryColor,
                    size: 20,
                  ),
                  SizedBox(width: 8),
                  Text(
                    '${userModel.address}',
                  ), // initial address value = "Puri Surya Jaya"
                ],
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ],
    ),
  );
}
