import 'package:flutter/material.dart';

Color mainColor = Color(0XFFff8080);
Color secondaryColor = Colors.indigo;
Color issuedComplaintColor = Colors.green;
Color closedComplaintColor = Colors.red;
Color inProgressComplaintColor = Colors.orange;

TextStyle regularTextStyle = TextStyle();
