import 'package:cloud_firestore/cloud_firestore.dart';

class StaffModel {
  String uid;
  String email;
  String fullName;
  String staffRole;
  String staffPhone;

  StaffModel(
      {this.uid, this.email, this.fullName, this.staffRole, this.staffPhone});

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'email': email,
        'fullName': fullName,
        'staffRole': staffRole,
        'staffPhone': staffPhone,
      };

  factory StaffModel.fromData(Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }

    return StaffModel(
      uid: data['uid'],
      fullName: data['fullName'],
      email: data['email'],
      staffRole: data['staffRole'],
      staffPhone: data['staffPhone'],
    );
  }

  StaffModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
    uid = doc.id;
    email = doc.data()['email'];
    fullName = doc.data()['fullName'];
    staffRole = doc.data()['staffRole'];
    staffPhone = doc.data()['staffPhone'];
  }

  factory StaffModel.fromDocument(DocumentSnapshot doc) {
    return StaffModel(
      email: doc['email'],
      fullName: doc['fullName'],
      staffRole: doc['staffRole'],
      staffPhone: doc['staffPhone'],
    );
  }
}
