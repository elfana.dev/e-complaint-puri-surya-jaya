import 'package:ecs_psj/model/user_model.dart';

class Complaints {
  final String id;
  final String complaintType;
  final String complaintSubType;
  final String complaintLocation;
  final String complaintTaman;
  final String complaintRemarks;
  final String complaintDate;
  final String complaintImageUrl;
  final String status;
  final String feedback;
  final ClientModel user;
  //final bool isCompleted;

  Complaints({
    this.id,
    this.complaintType,
    this.complaintSubType,
    this.complaintLocation,
    this.complaintTaman,
    this.complaintRemarks,
    this.complaintDate,
    this.complaintImageUrl,
    this.status,
    this.feedback,
    this.user,
    //this.isCompleted = false,
  });
}
