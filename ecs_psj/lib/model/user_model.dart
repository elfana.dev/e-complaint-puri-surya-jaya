import 'package:cloud_firestore/cloud_firestore.dart';

class ClientModel {
  String uid;
  String email;
  String fullName;
  String phoneNo;
  String address;

  ClientModel({this.uid, this.email, this.fullName, this.phoneNo, this.address});

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'email': email,
        'fullName': fullName,
        'phoneNo': phoneNo,
        'address': address,
      };

  factory ClientModel.fromData(Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    return ClientModel(
      uid: data['uid'],
      email: data['email'],
      fullName: data['fullName'],
      phoneNo: data['phoneNo'],
      address: data['address'],
    );
  }

  ClientModel.fromDocumentSnapshot({DocumentSnapshot doc}) {
    uid = doc.id;
    email = doc.data()['email'];
    fullName = doc.data()['fullName'];
    phoneNo = doc.data()['phoneNo'];
    address = doc.data()['address'];
  }

  factory ClientModel.fromDocument(DocumentSnapshot doc) {
    return ClientModel(
      email: doc['email'],
      fullName: doc['fullName'],
      phoneNo: doc['phoneNo'],
      address: doc['address'],
    );
  }
}
