# ecs_psj

Android-based Complaint Management System for Puri Surya Jaya Sidoarjo

FYP Objectives: 
1)	To identify all users and system for the management of complaint processes at the Puri Surya Jaya Sidoarjo in Indonesia.
2)	To propose, design, and develop a new android-based complaint management system for Puri Surya Jaya Sidoarjo.
3)	To validate the proposed android-based complaint management system by using survey to related stakeholders.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
